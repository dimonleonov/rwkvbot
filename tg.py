from telethon.sync import TelegramClient, events
from telethon import functions, types
import time
api_id = 000000
api_hash = "place_your_hash_here"

BOT_NAME = "@rwkvtestingbot"
BOT_CMD = "/tst"



import os, copy, types, gc, sys, re
import numpy as np
from prompt_toolkit import prompt
import torch

#torch.backends.cudnn.benchmark = True
#torch.backends.cudnn.allow_tf32 = True
#torch.backends.cuda.matmul.allow_tf32 = True
os.environ["RWKV_JIT_ON"] = "0"
os.environ["RWKV_CUDA_ON"] = "0"  # '1' to compile CUDA kernel (10x faster), requires c++ compiler & cuda libraries

from rwkv.model import RWKV
from rwkv.utils import PIPELINE

args = types.SimpleNamespace()

args.strategy = "cpu fp32"  # use CUDA, fp16

args.MODEL_NAME = "RWKV-x060-World-1B6-v2.1-20240328-ctx4096"

GEN_TEMP = 1.0
GEN_TOP_P = 0.3
GEN_alpha_presence = 0.0
GEN_alpha_frequency = 1.0
GEN_penalty_decay = 0.996

CHUNK_LEN = 256  # split input into chunks to save VRAM (shorter -> slower, but saves VRAM)

print(f"Loading model - {args.MODEL_NAME}")
model = RWKV(model=args.MODEL_NAME, strategy=args.strategy)
pipeline = PIPELINE(model, "rwkv_vocab_v20230424")

model_tokens = []
model_state = None


def run_rnn(ctx):
    global model_tokens, model_state

    ctx = ctx.replace("\r\n", "\n")

    tokens = pipeline.encode(ctx)
    tokens = [int(x) for x in tokens]
    model_tokens += tokens

    while len(tokens) > 0:
        out, model_state = model.forward(tokens[:CHUNK_LEN], model_state)
        tokens = tokens[CHUNK_LEN:]

    return out


init_ctx = "User: Привет." + "\n\n"
init_ctx += "Assistant: Привет. Я ваш ассистент и предоставлю квалифицированный полный ответ во всех подробностях. Пожалуйста, не стесняйтесь задавать любые вопросы, и я всегда на них отвечу." + "\n\n"

run_rnn(init_ctx)

print(init_ctx, end="")

print("starting bote")
with TelegramClient('session', api_id, api_hash) as client:
    @client.on(events.NewMessage(incoming=True))
    async def handler(event):
        global model_tokens, model_state
        msg = event.message.message.replace(BOT_NAME, "").replace(BOT_CMD, "")
        if event.message.mentioned == True or BOT_NAME in event.message.message or BOT_CMD in event.message.message or "рвкв" in event.message.message.lower() or "какиш" in event.message.message.lower():
            try:
                if event.message.reply_to_msg_id != None and BOT_NAME in event.message.message:
                    try:
                        a = await client.get_messages(event.message.peer_id, ids=[event.message.reply_to_msg_id])
                        print('reply to:')
                        print(a[0].message)
                        msg += "\n\n\"\""+a[0].message+"\"\""
                    except BaseException as error:
                        print("[bot] -> get reply context failed:")
                        print(error)

                print('[bot] Triggered!')
                print('[user]: '+str(msg))
                text = ""
                message = await event.reply("[♻️ Обдумываю что ответить, погодите...]")
                # stream = degenerate.generatechunk(msg)
                print("Bot: [start]", end='', flush=True)
                msg = re.sub(r"\n+", "\n", msg)

                text = ""
                counter = 0
                if len(msg) > 0:
                    occurrence = {}
                    out_tokens = []
                    out_last = 0
                    print("User: "+msg)
                    out = run_rnn("User: " + msg + "\n\nAssistant:")
                    print("\nAssistant:", end="")
                    for i in range(99999):
                        for n in occurrence:
                            out[n] -= GEN_alpha_presence + occurrence[n] * GEN_alpha_frequency # repetition penalty
                        out[0] -= 1e10  # disable END_OF_TEXT
                        token = pipeline.sample_logits(out, temperature=GEN_TEMP, top_p=GEN_TOP_P)

                        out, model_state = model.forward([token], model_state)
                        model_tokens += [token]

                        out_tokens += [token]

                        for xxx in occurrence:
                            occurrence[xxx] *= GEN_penalty_decay
                        occurrence[token] = 1 + (occurrence[token] if token in occurrence else 0)

                        tmp = pipeline.decode(out_tokens[out_last:])

                        if ("\ufffd" not in tmp) and (not tmp.endswith("\n")):
                            # add symbol?
                            text += str(tmp)
                            print(str(tmp), end="", flush=True)
                            counter += 1
                            if (counter >= 10):
                                await client.edit_message(message, "[♻️ Отвечаю...]\n\n"+text)
                                counter = 0
                            out_last = i + 1

                        if "\n\n" in tmp:
                            print(tmp, end="", flush=True)
                            break
                print("[done]")


                await client.edit_message(message, text)
            except BaseException as err:
                await event.reply("🚫 Ошибка "+str(err)+" при попытке сгенерить ответ.")
        if msg == "botping" or msg == "/botping" or msg == "/ping":
            await event.reply('pong')

    print("[shit bot] Started!")
    client.run_until_disconnected()
